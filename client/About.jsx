import React, {Component} from 'react';

export default class About extends Component {

	setVar() {
		Session.set('test', 'Hello');
	}
	
	render() {
		return(
			<div>
				<h1>About Us</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed dignissim nulla. Suspendisse quis ligula vel lacus ultrices dapibus quis consectetur augue. In imperdiet est vitae neque aliquam ullamcorper. Etiam felis felis, consectetur tristique commodo id, rhoncus viverra tortor. Aliquam erat volutpat. Sed mattis varius fermentum. Duis sapien risus, ultrices nec diam vitae, rutrum consectetur enim. Pellentesque lectus dui, placerat non ex ut, pharetra molestie sem. Vivamus blandit est vel ex euismod tristique. Morbi euismod nisl orci, nec auctor arcu commodo ut. Etiam ornare imperdiet fermentum. Maecenas et commodo nulla. Praesent consequat orci at posuere aliquam.</p>
				<button onClick={this.setVar}>Click Me</button>
			</div>
		)
	}
}